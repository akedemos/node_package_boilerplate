import myObject from '../src/index'
import { assert } from 'chai'

describe('module', () => {
  it('should have the correct properties', () => {
    assert.equal('package starter kit', myObject.name)
  })
})
