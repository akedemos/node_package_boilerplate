[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

# Using this Boilerplate

This repo is intended for use creating node modules designed to be imported by other services. For apps and scripts
(React, Web, Phonegap, Heroku, command line apps etc) Please find the appropriate repository for your application and
use that, if it doesn't exist fork this repo and create it.

To use simply clone the repo

```sh
git clone git@bitbucket.org:akedemos/node_package_boilerplate.git
```

Then clear your working tree by removing the .git directory

# Boilerplate Setup Tasks

Complete the following tasks IN ORDER!

### Package.json
- [ ] TODO: Change package name (Don't forget any scope prefix!)
- [ ] TODO: Reset the version
- [ ] TODO: Update the description
- [ ] TODO: Update the git url
- [ ] TODO: Change the homepage url

### Continuous Integration

- [ ] TODO: create new project
- [ ] TODO: set up deployment pipeline
- [ ] TODO: add the codeship badge


TODO: add the next steps here ...

# Package Name

TODO: Write a project description

## Installation

TODO: Describe the installation process

## Usage

TODO: Write usage instructions

## Structure

TODO: show src directory structure 

## History

TODO: Write history

## License

All Rights Reserved
